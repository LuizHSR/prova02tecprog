package utfpr.ct.dainf.pratica;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 *
 * @author
 */
public class Ponto {
    private double x, y, z;

    /**
     * Retorna o nome não qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public Ponto()
    {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }
    public Ponto(double xi, double yi, double zi)
    {
        this.x = xi;
        this.y = yi;
        this.z = zi;
    }
    public double getX()
    {
        return x;
    }
    public double getY()
    {
        return y;
    }
    public double getZ()
    {
        return z;
    }
    public void setX(double xk)
    {
        x = xk;
    }
    public void setY(double yk)
    {
        x = yk;
    }
    public void setZ(double zk)
    {
        x = zk;
    }
    public String getNome()
    {
        return getClass().getSimpleName();
    }
    @Override
    public String toString()
    {
        String res;
        Double aux;
        
        res = this.getNome();
        aux = this.getX();
        res = res + "(" + aux + ",";
        aux = this.getY();
        res = res + aux + ",";
        aux = this.getZ();
        res = res + aux + ")";
        
        return res;
    }
    @Override
    public boolean equals(Object o)
    {
        boolean res = false;
        if(o instanceof Ponto)
        {
            if(this.getX() == ((Ponto) o).getX() && this.getY() == ((Ponto) o).getY() && this.getZ() == ((Ponto) o).getZ())
                res = true;
        }
        return res;
    }
    public double dist(Ponto dot)
    {
        double res;
        double aux;
        
        aux = (Math.pow(dot.getX() - this.getX(),2)) + (Math.pow(dot.getY() - this.getY(),2)) + (Math.pow(dot.getZ() - this.getZ(),2));
        res = Math.sqrt(aux);
        return res;
    }

}
