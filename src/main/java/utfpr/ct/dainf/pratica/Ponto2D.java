/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1795406
 */
public abstract class Ponto2D extends Ponto {
    protected Ponto2D()
    {
        super();
    }
    protected Ponto2D(double xi, double yi, double zi)
    {
        super(xi,yi,zi);
    }
}
