/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1795406
 */
public class PontoYZ extends Ponto2D{
    
    public PontoYZ()
    {
        super();
    }
    public PontoYZ(double yl, double zl)
    {
        super(0, yl, zl);
    }
    @Override
    public String toString()
    {
        String res;
        Double aux;
        
        res = this.getNome();
        aux = this.getY();
        res = res + "(" + aux + ",";
        aux = this.getZ();
        res = res + aux + ")";
        
        return res;
    }
}
