/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1795406
 */
public class PontoXZ extends Ponto2D{
    
    public PontoXZ()
    {
        super();
    }
    public PontoXZ(double xl, double zl)
    {
        super(xl, 0, zl);
    }
    
    @Override
    public String toString()
    {
        String res;
        Double aux;
        
        res = this.getNome();
        aux = this.getX();
        res = res + "(" + aux + ",";
        aux = this.getZ();
        res = res + aux + ")";
        
        return res;
    }
}
