/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1795406
 */
public class PontoXY extends Ponto2D{
    
    public PontoXY()
    {
        super();
    }
    public PontoXY(double xl, double yl)
    {
        super(xl, yl, 0);
    }
    @Override
    public String toString()
    {
        String res;
        Double aux;
        
        res = this.getNome();
        aux = this.getX();
        res = res + "(" + aux + ",";
        aux = this.getY();
        res = res + aux + ")";
        
        return res;
    }
}
