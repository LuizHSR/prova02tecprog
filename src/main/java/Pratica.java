
import utfpr.ct.dainf.pratica.Ponto;
import utfpr.ct.dainf.pratica.PontoXY;
import utfpr.ct.dainf.pratica.PontoXZ;
import utfpr.ct.dainf.pratica.PontoYZ;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
public class Pratica {

    public static void main(String[] args) {
        Ponto pxz = new PontoXZ(-3,2);
        Ponto pxy = new PontoXY(0,2);
        
        double resDist = pxz.dist(pxy);
        
        System.out.println("Distancia = " + resDist);
        
    }
    
}
